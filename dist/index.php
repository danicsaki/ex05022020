<!doctype html>
<html>

<head>
    <title>Add title here</title>
    <script src="https://kit.fontawesome.com/7202d9a129.js" crossorigin="anonymous"></script>
</head>

<body>

    <?php
        include 'db_connection.php';
?>

    <!-- NAVBAR -->
    <div class="">
        <nav class="navbar navbar-expand-lg sticky-top navbar-dark bg-dark">
            <a class="navbar-brand" href="#"><img src="./images/logo@2x.png" class="logo" alt="logo"></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown"
                aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavDropdown">
                <ul class="navbar-nav">
                    <li class="nav-item active">
                        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item dropdown active">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Pages
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <a class="dropdown-item" href="#">About Us</a>
                            <a class="dropdown-item" href="#">Menu</a>
                            <a class="dropdown-item" href="#">News & Events</a>
                            <a class="dropdown-item" href="#">Chefs</a>
                            <a class="dropdown-item" href="#">Coming soon</a>
                            <a class="dropdown-item" href="#">Error 404</a>
                        </div>
                    </li>
                    <li class="nav-item dropdown active">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Portofolio
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <a class="dropdown-item" href="#">Portofolio 2 Columns</a>
                            <a class="dropdown-item" href="#">Portofolio 3 Columns</a>
                            <a class="dropdown-item" href="#">Portofolio 4 Columns</a>
                        </div>
                    </li>
                    <li class="nav-item dropdown active">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Blog
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <a class="dropdown-item" href="#">Blog With Right Sidebar</a>
                            <a class="dropdown-item" href="#">Blog With Left Sidebar</a>
                            <a class="dropdown-item" href="#">Article Category Blog</a>
                        </div>
                    </li>
                    <li class="nav-item dropdown active">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Post Formats
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <a class="dropdown-item" href="#">Standard Post Format</a>
                            <a class="dropdown-item" href="#">Video Post Format</a>
                            <a class="dropdown-item" href="#">Audio Post Format</a>
                            <a class="dropdown-item" href="#">Gallery Post Format</a>
                            <a class="dropdown-item" href="#">Link Post Format</a>
                            <a class="dropdown-item" href="#">Status Post Format</a>
                            <a class="dropdown-item" href="#">Quote Post Format</a>
                            <a class="dropdown-item" href="#">Image Post Format</a>
                        </div>
                    </li>
                    <li class="nav-item dropdown active">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Joomla!
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <a class="dropdown-item" href="#">Action</a>
                            <a class="dropdown-item" href="#">Another action</a>
                            <a class="dropdown-item" href="#">Something else here</a>
                        </div>
                    </li>
                    <li class="nav-item dropdown active">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            K2 Blog
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <a class="dropdown-item" href="#">Blog Listing</a>
                            <a class="dropdown-item" href="#">Single Blog</a>
                            <a class="dropdown-item" href="#">User's Blog</a>
                        </div>
                    </li>
                </ul>

            </div>
        </nav>
    </div>

    <!-- WELCOME -->
    <div class="welcome my-5">
        <blockquote class="blockquote text-center mb-5">
            <p class="mb-3 h1 welcome-title">Welcome To At Restaurant</p>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
        </blockquote>

        <div class="row m-3">
            <div class="col mx-5">
                <i class="fas fa-utensils mb-4"></i>
                <div>
                    <h3>Best Cuisine</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.ullamcorper diam nec augue semper, in
                        dignissim.</p>
                </div>
            </div>
            <div class="col mx-5">
                <i class="fas fa-glass-martini mb-4"></i>
                <div>
                    <h3>Special Offers</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.ullamcorper diam nec augue semper, in
                        dignissim.</p>
                </div>
            </div>
            <div class="col mx-5">
                <i class="fas fa-beer mb-4"></i>
                <div>
                    <h3>Good Rest</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.ullamcorper diam nec augue semper, in
                        dignissim.</p>
                </div>
            </div>
            <div class="col mx-5">
                <i class="fas fa-coffee mb-4"></i>
                <div>
                    <h3>Timings</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.ullamcorper diam nec augue semper, in
                        dignissim.</p>
                </div>
            </div>
        </div>
    </div>

    <!-- LAST WORK -->
    <div class=" my-5">
        <blockquote class="blockquote text-center mb-5">
            <p class="mb-3 h1 welcome-title">Latest Work</p>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
        </blockquote>

        <div class="container">
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#home">All</a></li>
                <li><a data-toggle="tab" href="#menu1">Appetizers & Soups</a></li>
                <li><a data-toggle="tab" href="#menu2">Sauces & Sides</a></li>
                <li><a data-toggle="tab" href="#menu3">Desserts</a></li>
                <li><a data-toggle="tab" href="#menu4">Other</a></li>
            </ul>

            <div class="tab-content">
                <div id="home" class="tab-pane fade in active">
                    <div class="row">
                        <?php

                            $result = mysqli_query($conn,"SELECT * FROM produse");

                            while($row = mysqli_fetch_array($result))
                            {
                            
                            echo '<div class="card container col-3" style="width: 18rem;">';
                            echo '<img src="./images/'. $row["image"] . '" class="card-img-top" alt="...">';
                            echo '<div class="card-body m-auto">
                            <i class="fas fa-link"></i>
                            <i class="far fa-image"></i>';
                            echo '<h5 class="card-title">' . $row["nume"] . '</h5></div></div>';
                            }
                        ?>
                    </div>

                </div>
            
            <div id="menu1" class="tab-pane fade">
                <div class="row">
                    <?php

                        $result = mysqli_query($conn,"SELECT * FROM produse WHERE categorie='Sauces & Sides'");

                        while($row = mysqli_fetch_array($result))
                        {
                        
                        echo '<div class="card container col-3" style="width: 18rem;">';
                        echo '<img src="./images/'. $row["image"] . '" class="card-img-top" alt="...">';
                        echo '<div class="card-body m-auto">
                        <i class="fas fa-link"></i>
                        <i class="far fa-image"></i>';
                        echo '<h5 class="card-title">' . $row["nume"] . '</h5></div></div>';
                        }
                    ?>
                </div>
            </div>
        
        <div id="menu2" class="tab-pane fade">
            <div class="row">
                <?php

        $result = mysqli_query($conn,"SELECT * FROM produse WHERE categorie='Appetisers & Soups'");

        while($row = mysqli_fetch_array($result))
        {
        
        echo '<div class="card container col-3" style="width: 18rem;">';
        echo '<img src="./images/'. $row["image"] . '" class="card-img-top" alt="...">';
        echo '<div class="card-body m-auto">
        <i class="fas fa-link"></i>
        <i class="far fa-image"></i>';
        echo '<h5 class="card-title">' . $row["nume"] . '</h5></div></div>';
        }
      ?>
            </div>
        </div>
        <div id="menu3" class="tab-pane fade">
            <div class="row">
                <?php

        $result = mysqli_query($conn,"SELECT * FROM produse WHERE categorie='Desserts'");

        while($row = mysqli_fetch_array($result))
        {
        
        echo '<div class="card container col-3" style="width: 18rem;">';
        echo '<img src="./images/'. $row["image"] . '" class="card-img-top" alt="...">';
        echo '<div class="card-body m-auto">
        <i class="fas fa-link"></i>
        <i class="far fa-image"></i>';
        echo '<h5 class="card-title">' . $row["nume"] . '</h5></div></div>';
        }
      ?>
            </div>
        </div>
        <div id="menu4" class="tab-pane fade">
            <div class="row">
                <?php

        $result = mysqli_query($conn,"SELECT * FROM produse WHERE categorie='Other'");

        while($row = mysqli_fetch_array($result))
        {
        
        echo '<div class="card container col-3" style="width: 18rem;">';
        echo '<img src="./images/'. $row["image"] . '" class="card-img-top" alt="...">';
        echo '<div class="card-body m-auto">
        <i class="fas fa-link"></i>
        <i class="far fa-image"></i>';
        echo '<h5 class="card-title">' . $row["nume"] . '</h5></div></div>';
        }
      ?>
            </div>
        </div>
    </div>
    </div>

    </div>

    <script src="https://code.jquery.com/jquery-3.1.0.js"></script>
    <script src="main.js"></script>
</body>

</html>