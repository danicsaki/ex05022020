-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Feb 05, 2020 at 09:51 AM
-- Server version: 5.7.26
-- PHP Version: 7.3.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `web_examen_sem_2`
--

-- --------------------------------------------------------

--
-- Table structure for table `produse`
--

CREATE TABLE `produse` (
  `id` int(255) NOT NULL,
  `nume` varchar(255) NOT NULL,
  `categorie` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `produse`
--

INSERT INTO `produse` (`id`, `nume`, `categorie`, `image`) VALUES
(1, 'Portofolio 3', 'Appetisers & Soups', 'portfolio3_600x600_1461922506_jpg_ef6c379dcf988188b71efefa18af096b.jpg'),
(2, 'Portofolio 5', 'Appetisers & Soups', 'portfolio5_600x600_1461922506_jpg_3ad81279ba7e6fc8ccc2f6e218b6547c.jpg'),
(3, 'Portofolio 6', 'Sauces & Sides', 'portfolio6_600x600_1461922507_jpg_4e214bbc04e90270d3a87800627dbe7d.jpg'),
(4, 'Portofolio 7', 'Sauces & Sides', 'portfolio7_600x600_1461922507_jpg_5c4bb89ef469312bd22f32bf77d0a049.jpg'),
(5, 'Portofolio 8', 'Desserts', 'portfolio8_600x600_1461922507_jpg_317b3fe078b34e73b00d8aab698df8aa.jpg'),
(6, 'Portofolio 9', 'Desserts', 'portfolio9_600x600_1461922507_jpg_e808619c4eb8e7ad160b6c0d4a26e685.jpg'),
(7, 'Portofolio 4', 'Other', 'portfolio14_600x600_1461922508_jpg_f201a68966391ecc44d59452d5c86ad8.jpg'),
(8, 'Portofolio 15', 'Other', 'portfolio15_600x600_1461922508_jpg_46bc84107cf0a91e544836c07a6a2b61.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `produse`
--
ALTER TABLE `produse`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `produse`
--
ALTER TABLE `produse`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
